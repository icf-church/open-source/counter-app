# Counter app

A simple app to count people in different rooms.

## Contact
Product Owner: Simon Egli (simon.egli@icf.ch)

## Development
Docker provides all the necessary tooling for local development.

### 1. Create .env
Copy the .env.example and add the variables.
See chapter deployment on how to get the Firebase login token.
See next chapter on how to get the Firebase config.

### 2. Setup Firebase
- Go to https://firebase.google.com/ and create a new Firebase project.
- Create a new web app with Firebase hosting enabled.
- Get the SDK configuration and add it to the .env file
- Rename .firebaserc.example to .firebaserc and add your project id:

```js
{
  "projects": {
    "default": "<your-app-id>"
  }
}
```
Hint: You could also use the Firebase CLI to init the project if your are familiar with Firebase:
```bash
$ docker compose run --rm node firebase login --no-localhost && firebase init
```

### 3. Install the dependencies
```bash
docker compose run --rm node yarn
```

### 4. Start dev server
```bash
$ yarn dev
# docker compose run --rm -p 8080:8080 node quasar dev
```

## TODO
- Add documentation for complete firebase authentication

## Deployment
Before you can deploy you need a Firebase login token. Follow the steps below to get one:
``` bash
# Get the Firebase login token
$ docker compose run --rm node firebase login:ci --no-localhost
```

``` bash
# Add the login token to the .env file
FIREBASE_TOKEN=[paste the code here]
```

Copy the .firebaserc.example and add your own project id to projects -> default. You can see your project id via:
``` bash
firebase projects:list
```

``` bash
# Build the frontend and push to Firebase
$ yarn release
# source .env && docker compose run --rm node quasar build -m pwa && docker compose run --rm node firebase deploy --token $FIREBASE_TOKEN
```

There is also a gitlab-ci.yml file for your Gitlab CI/CD pipeline to deploy automatically.
Add the content of your .firebaserc as a ci/cd variable (type: file). This file is used in the pipeline.

## Configuration
There are some configuration available in the counter app:

- Data structure: `Communities -> Times -> Rooms`
- You can define the data structure here: `/src/pages/Reset.vue`.
- Add the ID's of your admin users here: `./src/store/module-counter/state.js` (You will find them on Firebase in the authentication module.)

## Requirements
- Docker
- Git

## Tech Stack
- https://quasar.dev/
- https://vuejs.org/
- https://firebase.google.com/
