import { getAuth, signOut } from "firebase/auth";

export function logout({ state, commit }) {

  return new Promise((resolve, reject) => {

    const auth = getAuth();

    signOut(auth).then(() => {
      commit('SET_USER', null);
      resolve();
    }).catch((error) => {
      reject(error);
    });

  });
}
