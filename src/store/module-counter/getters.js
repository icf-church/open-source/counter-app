export function getUser (state) {
    return state.user;
}

export function isAdmin (state) {
    return (state.user && state.adminUis.includes(state.user.uid));
}

