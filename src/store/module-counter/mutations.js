export function SET_USER (state, user) {
  state.user = user;
}

export function SET_ADMINS (state, admins) {
  state.adminUis = admins;
}
