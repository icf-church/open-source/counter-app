import { boot } from 'quasar/wrappers'
import axios from 'axios'

// Be careful when using SSR for cross-request state pollution
// due to creating a Singleton instance here;
// If any client changes this (global) instance, it might be a
// good idea to move this instance creation inside of the
// "export default () => {}" function below (which runs individually
// for each client)
const apiChurchMetrics = axios.create({
  baseURL: process.env.CM_BASE_URL,
  headers: {
    common: {
      'X-Auth-User': process.env.CM_AUTH_USER,
      'X-Auth-Key': process.env.CM_AUTH_KEY
    }
  }
})

export default boot(({ app }) => {

  // for use inside Vue files (Options API) through this.$axios and this.$api
  app.config.globalProperties.$axios = axios
  // ^ ^ ^ this will allow you to use this.$axios (for Vue Options API form)
  //       so you won't necessarily have to import axios in each vue file

  app.config.globalProperties.$apiChurchMetrics = apiChurchMetrics
  // ^ ^ ^ this will allow you to use this.$api (for Vue Options API form)
  //       so you can easily perform requests against your app's API
})

export { apiChurchMetrics }
