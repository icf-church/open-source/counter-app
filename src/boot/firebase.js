import { boot } from 'quasar/wrappers'

// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import { getAuth, onAuthStateChanged } from "firebase/auth";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

export default boot(async ({ app, router, store }) => {

  const firebaseConfig = {
    apiKey: process.env.API_KEY,
    authDomain: process.env.AUTH_DOMAIN,
    databaseURL: process.env.DATABASE_URL,
    projectId: process.env.PROJECT_ID,
    storageBucket: process.env.STORAGE_BUCKET,
    messagingSenderId: process.env.MESSAGING_SENDER_ID,
    appId: process.env.APP_ID,
    measurementId: process.env.MEASUREMENT_ID
  };

  // Initialize Firebase
  const firebase = initializeApp(firebaseConfig);
  const auth = getAuth();

  // See also: https://dev.to/gautemeekolsen/vue-guard-routes-with-firebase-authentication-f4l
  const getCurrentUser = () => {
    return new Promise((resolve, reject) => {
      const unsubscribe = onAuthStateChanged(auth, (userFirebase) => {
        unsubscribe()
        resolve(userFirebase)
      }, reject)
    })
  }

  // On a page reload we fetch the current user and store it in the state
  const user = await getCurrentUser();
  store.commit('counter/SET_USER', user);
  
  const analytics = getAnalytics(firebase);

  // See also: https://router.vuejs.org/guide/advanced/navigation-guards.html
  router.beforeEach(async (to, from, next) => {

    const requiresAuth = to.matched.some(record => record.meta.requiresAuth);

    if (requiresAuth && !await getCurrentUser()){
      next({ name: 'login' });
    } else{
      next();
    }
  });
});
