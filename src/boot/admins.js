import { boot } from 'quasar/wrappers'
import dataStructure from '../../config/structure.json';

// "async" is optional;
// more info on params: https://v2.quasar.dev/quasar-cli/boot-files
export default boot(async ({ app, router, store }) => {

  let adminIds = [];

  dataStructure['communities'].forEach((community) => {
    community.adminIds.forEach((admin) => {
      if (adminIds.indexOf(admin) === -1) {
        adminIds.push(admin);
      }
    })
  })

  store.commit('counter/SET_ADMINS', adminIds);
})
