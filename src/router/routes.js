
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        name: 'home',
        path: '',
        component: () => import('pages/Index.vue')
      },
      {
        name: 'reset',
        path: 'reset',
        component: () => import('pages/Reset.vue'),
        meta: { requiresAuth: true }
      },
      {
        name: 'statsCommunity',
        path: 'community/:idCommunity/stats',
        props: true,
        component: () => import('pages/StatsCommunity.vue'),
        meta: { requiresAuth: true }
      },
      {
        name: 'community',
        path: 'community/:idCommunity',
        component: () => import('pages/Community.vue'),
        props: true
      },
      {
        name: 'time',
        path: 'community/:idCommunity/time/:idTime',
        props: true,
        component: () => import('pages/Time.vue')
      },
      {
        name: 'room',
        path: 'community/:idCommunity/time/:idTime/room/:idRoom',
        props: true,
        component: () => import('pages/Room.vue')
      },
      {
        name: 'login',
        path: 'login',
        component: () => import('pages/Login.vue')
      },
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
