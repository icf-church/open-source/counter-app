# 1. DEVELOP STAGE
FROM node:18-alpine AS develop

WORKDIR /app

RUN yarn global add @quasar/cli
RUN yarn global add firebase-tools
